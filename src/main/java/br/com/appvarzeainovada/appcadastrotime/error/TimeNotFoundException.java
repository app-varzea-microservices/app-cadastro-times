package br.com.appvarzeainovada.appcadastrotime.error;

@SuppressWarnings("serial")
public class TimeNotFoundException extends RuntimeException{
	
	public TimeNotFoundException(Integer id){
		super(String.format("Nenhum Time encontrado pelo id: <%s>", id));
	}

}
