package br.com.appvarzeainovada.appcadastrotime.enums;

import lombok.Getter;

@Getter
public enum EnumTipoEquipe {

	FEMININO(1),
	MASCULINO(2);
	
	private int tipoEquipe;
	
	private EnumTipoEquipe(int tipoEquipe){
		this.tipoEquipe = tipoEquipe;
	}
}
