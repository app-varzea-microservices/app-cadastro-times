package br.com.appvarzeainovada.appcadastrotime.enums;

import lombok.Getter;

@Getter
public enum EnumModalidade {

	CAMPO(1),
	FUTSAL(2),
	SOCIETY(3);
	
	private int modalidade;
	
	private EnumModalidade(int modalidade){
		this.modalidade = modalidade;
	}
}
