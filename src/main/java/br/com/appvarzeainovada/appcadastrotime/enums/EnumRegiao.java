package br.com.appvarzeainovada.appcadastrotime.enums;

import lombok.Getter;

@Getter
public enum EnumRegiao {

	LESTE(1),
	OESTE(2),
	SUl(3),
	NORTE(4);
	
	private int regiao;
	
	private EnumRegiao(int regiao){
		this.regiao = regiao;
	}
	
}
