package br.com.appvarzeainovada.appcadastrotime.enums;

import lombok.Getter;

@Getter
public enum EnumCategoria {

	VETERANO(1),
	ESPORTE(2),
	MASTER(3);
	
	private int categoria;
	
	private EnumCategoria(int categoria){
		this.categoria = categoria;
	}
}
