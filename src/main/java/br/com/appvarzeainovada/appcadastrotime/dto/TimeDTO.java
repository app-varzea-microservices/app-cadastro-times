package br.com.appvarzeainovada.appcadastrotime.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.appvarzeainovada.appcadastrotime.enums.EnumModalidade;
import br.com.appvarzeainovada.appcadastrotime.enums.EnumRegiao;
import br.com.appvarzeainovada.appcadastrotime.enums.EnumTipoEquipe;
import br.com.appvarzeainovada.appcadastrotime.model.Time;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class TimeDTO {

	private Integer id;
	
	@NotEmpty
	@Size(max = Time.MAX_LENGTH_NOME)
	private String nome;
	
	@NotNull
	private EnumModalidade modalidade;

	@NotNull
	private EnumRegiao regiao;

	@NotNull
	private EnumTipoEquipe tipoEquipe; 
	
	@NotNull
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private UsuarioDTO usuario;
	
}
