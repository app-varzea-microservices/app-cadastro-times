package br.com.appvarzeainovada.appcadastrotime.dto;

import javax.validation.constraints.NotEmpty;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDTO {
	
	private Integer id;

	@NotEmpty
	private String login;
	
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String senha;
}
