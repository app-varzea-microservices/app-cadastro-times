package br.com.appvarzeainovada.appcadastrotime.service.impl;

import java.util.List;

import br.com.appvarzeainovada.appcadastrotime.dto.TimeDTO;

public interface TimeServiceImpl {

	TimeDTO create (TimeDTO time);
	
	TimeDTO delete (Integer id);
	
	List<TimeDTO> findAll();
	
	TimeDTO findById(Integer id);
	
	TimeDTO update (TimeDTO time);
}
