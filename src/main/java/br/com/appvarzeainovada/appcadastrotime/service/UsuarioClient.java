package br.com.appvarzeainovada.appcadastrotime.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.appvarzeainovada.appcadastrotime.dto.UsuarioDTO;

@FeignClient(name = "usuario-auth", url = "localhost:9093/usuario")
public interface UsuarioClient {

	 @PostMapping
	 UsuarioDTO cadastrar(@RequestBody UsuarioDTO usario);

}
