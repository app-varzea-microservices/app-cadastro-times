package br.com.appvarzeainovada.appcadastrotime.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.appvarzeainovada.appcadastrotime.dto.TimeDTO;
import br.com.appvarzeainovada.appcadastrotime.error.TimeNotFoundException;
import br.com.appvarzeainovada.appcadastrotime.model.Time;
import br.com.appvarzeainovada.appcadastrotime.repository.TimeRepository;
import br.com.appvarzeainovada.appcadastrotime.service.impl.TimeServiceImpl;


@Service
public final class TimeService implements TimeServiceImpl{

	@Autowired
	private TimeRepository repository;

	@Override
	public TimeDTO create(TimeDTO time) {
		Time persisted = Time.getBuilder()
				.nome(time.getNome())
				.regiao(time.getRegiao())
				.modalidade(time.getModalidade())
				.tipoEquipe(time.getTipoEquipe())
				.build();
		persisted = repository.save(persisted);
		
		return convertToDTO(persisted);
	}

	@Override
	public TimeDTO delete(Integer id) {
		Time deleted = findTimeById(id);
		repository.delete(deleted);
		return convertToDTO(deleted);
	}

	@Override
	public List<TimeDTO> findAll() {
		List<Time> timeEntries = repository.findAll();
		return convertToDTOs(timeEntries);
	}

	@Override
	public TimeDTO findById(Integer id) {
		Time found = findTimeById(id);
		return convertToDTO(found);
	}

	@Override
	public TimeDTO update(TimeDTO time) {
		Time updated = findTimeById(time.getId());
		updated.update(time.getNome(), time.getModalidade(), time.getRegiao(), time.getTipoEquipe());
		updated = repository.save(updated);
		
		return convertToDTO(updated);
	}
	
	private Time findTimeById(Integer id){
		Optional<Time> result = repository.findById(id);
		return result.orElseThrow(() -> new TimeNotFoundException(id));
	}
	
	public List<TimeDTO> convertToDTOs(List<Time> models){
		return models.stream()
				.map(this::convertToDTO)
				.collect(toList());
	}
	
	public TimeDTO convertToDTO (Time model){
		TimeDTO dto = new TimeDTO();
		
		dto.setId(model.getId());
		dto.setNome(model.getNome());
		dto.setModalidade(model.getModalidade());
		dto.setRegiao(model.getRegiao());
		dto.setTipoEquipe(model.getTipoEquipe());
		
		return dto;
	}
}
