package br.com.appvarzeainovada.appcadastrotime.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import br.com.appvarzeainovada.appcadastrotime.model.Time;

public interface TimeRepository extends JpaRepository<Time, Integer>{

}
