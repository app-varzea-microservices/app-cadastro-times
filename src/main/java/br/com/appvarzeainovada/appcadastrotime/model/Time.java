package br.com.appvarzeainovada.appcadastrotime.model;

import static util.PreCondition.isTrue;
import static util.PreCondition.notEmpty;
import static util.PreCondition.notNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import br.com.appvarzeainovada.appcadastrotime.enums.EnumModalidade;
import br.com.appvarzeainovada.appcadastrotime.enums.EnumRegiao;
import br.com.appvarzeainovada.appcadastrotime.enums.EnumTipoEquipe;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
public final class Time {

	public static final int MAX_LENGTH_NOME = 100;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Integer id;

	private String nome;

	private EnumModalidade modalidade;

	private EnumRegiao regiao;

	private EnumTipoEquipe tipoEquipe;

	public Time(){}

	public Time(Builder builder){
		super();
		this.nome = builder.nome;
		this.modalidade = builder.modalidade;
		this.regiao = builder.regiao;
		this.tipoEquipe = builder.tipoEquipe;
	}

	public void update(String nome, EnumModalidade modalidade, EnumRegiao regiao, EnumTipoEquipe tipoEquipe){
		checkObject(nome);
		this.nome = nome;
		this.modalidade = modalidade;
		this.regiao = regiao;
		this.tipoEquipe = tipoEquipe;
	}

	public static Builder getBuilder() {
		return new Builder();
	}

	@NoArgsConstructor
	public static class Builder {

		private String nome;

		public Builder nome (String nome){
			this.nome = nome;
			return this;
		}

		private EnumModalidade modalidade;

		public Builder modalidade(EnumModalidade modalidade){
			this.modalidade = modalidade;
			return this;
		}

		private EnumRegiao regiao;

		public Builder regiao(EnumRegiao regiao){
			this.regiao = regiao;
			return this;
		}

		private EnumTipoEquipe tipoEquipe;

		public Builder tipoEquipe(EnumTipoEquipe tipoEquipe){
			this.tipoEquipe = tipoEquipe;
			return this;
		}


		public Time build(){

			Time build = new Time(this);
			build.checkObject(build.getNome());

			return build;
		}
	}

	private void checkObject(String nome) {
		notNull(nome, "Nome não pode ser nulo");
		notEmpty(nome, "Nome não pode estar vazio");
		isTrue(nome.length() <= MAX_LENGTH_NOME,
				"Nome cannot be longer than %d characters",
				MAX_LENGTH_NOME
				);

	}
}
