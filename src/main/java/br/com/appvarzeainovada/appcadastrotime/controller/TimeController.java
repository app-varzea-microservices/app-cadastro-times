package br.com.appvarzeainovada.appcadastrotime.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.appvarzeainovada.appcadastrotime.dto.TimeDTO;
import br.com.appvarzeainovada.appcadastrotime.error.TimeNotFoundException;
import br.com.appvarzeainovada.appcadastrotime.service.TimeService;
import br.com.appvarzeainovada.appcadastrotime.service.UsuarioClient;


@RestController
@RequestMapping("/time")
public final class TimeController {

	@Autowired
	private TimeService service;
	
	@Autowired
	private UsuarioClient client;
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	TimeDTO create (@RequestBody @Valid TimeDTO timeEntry){
		
		client.cadastrar(timeEntry.getUsuario());
		
		return service.create(timeEntry);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	TimeDTO delete (@PathVariable("id") Integer id){
		return service.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	List<TimeDTO> findAll(){
		return service.findAll();
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	TimeDTO findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	TimeDTO update(@RequestBody @Valid TimeDTO timeEntry){
		return service.update(timeEntry);
	}
	
	 @ExceptionHandler
	 @ResponseStatus(HttpStatus.NOT_FOUND)
	 public ResponseEntity<?> handleNotFound(TimeNotFoundException ex){
		 return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	 }
}	

