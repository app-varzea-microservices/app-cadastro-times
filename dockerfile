FROM openjdk:8-jre
RUN mkdir app
ARG JAR_FILE
ADD /target/${JAR_FILE} /app/app-cadastro-time-0.0.1-SNAPSHOT.jar
WORKDIR /app
ENTRYPOINT java -jar app-cadastro-time-0.0.1-SNAPSHOT.jar